---
layout: handbook-page-toc
title: Slack Training
description: "Details on how to sign up for an in-app Slack training for new team members at GitLab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Sign-up

Please review the learning objectives, requirements, and training details before signing up.

Sign up for the Slack training by completing [this Google form](https://docs.google.com/forms/d/e/1FAIpQLSe1pBLLFYOuoAyKssl9BtVGNlQXP08YYcudrlsKu0lic6apWQ/viewform?usp=sf_link). After completing the form, the Learning and Development team will reach out with next steps.

## Testimonials

**Results**

1. 80% of learners agree that their confidence in successfully using Slack at GitLab increased after completing this training.
1. 90% of learners agree that the training helped them feel empowered with knowledge and resources needed to to speak up for GitLab values and correct usage of Slack.

**What learners are saying:**

`This training was great and I'm glad I was able to participate! If this training is not already part of onboarding for new team members, it should be considered.`

`It was a great refresher for me!`

### Is this training right for me?

This training is built for:

1. New team members
1. Team members who want a refresher on using Slack

This training is **not** built for:

1. Super experts in Slack: the training is focused on Slack basics and their connection to GitLab values.
1. Learners looking for advanced used of Slack commands and automations (follow along with planning for[Slack Training 2.0](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/356) for more advanced resources)


## Learning Objectives

GitLab team members participating in this training will:

1. Build confidence in their ability to use Slack as a form of informal and formal communication at GitLab.
1. Improve efficiency using tools built into the Slack app.
1. Contribute to reaching our target of [percent of messages that are not DMs](/handbook/communication/#why-we-track--of-messages-that-are-not-dms).
1. Review and reinforce GitLab values within the scope of Slack.

## Requirements

1. You're a GitLab team member (this is not available to external learners)
1. You can commit ~5 minutes per day for 10 business days to review and take action on tasks in Slack
1. You commit to completeing a post-training Google form to provide feedback on your experience

No prior experience with Slack or GitLab is needed to participate. The content reviewed is directed to brand new users in the GitLab Slack instance, however, team members are welcome to participate regardless of time at the company.

If you meet these requirements, please use [this Google form](https://docs.google.com/forms/d/e/1FAIpQLSe1pBLLFYOuoAyKssl9BtVGNlQXP08YYcudrlsKu0lic6apWQ/viewform?usp=sf_link) to sign-up.

## Training Details

Here's what to expect in the Slack training:

1. After signing up, you'll be added to a training cohort Slack group and the [#slack-training Slack channel](https://app.slack.com/client/T02592416/C02MX7LTXK9/thread/G018JT50VH7-1641496150.005700). The current goal is to run at least one cohort per month.
1. The training will run for 10 business days
1. Each day, you'll get one Slack group ping with your daily task instructions, posted publically to the [#slack-training channel](https://app.slack.com/client/T02592416/C02MX7LTXK9/thread/G018JT50VH7-1641496150.005700)
1. Messages will be scheduled by the L&D team and are triggered to send at 3pm UTC

## Metrics

We measure success using the following metrics:

| Metrics | How we measure | Goal |
| ----- | ----- | ----- |
| Percentage of new team members who complete the training per quarter | Completions of end of program survey | 30% |
|  Impact on target of [percent of messages that are not DMs](/handbook/communication/#why-we-track--of-messages-that-are-not-dms) | Tracked on [Slack handbook page](/handbook/communication/#why-we-track--of-messages-that-are-not-dms) | Correlation of course completions and target |

## Future improvements

The following topics will be prioritized in future iterations of this training:

1. Increase automation - explore tools and plugins that can automatically assign and trigger this training based on team member start date
1. Improve metrics - track impact of training on team member behavior in Slack
1. Create a [Slack training 2.0](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/356)

## Questions

Reach out in the [#learninganddevelopment Slack channel](https://app.slack.com/client/T02592416/CMRAWQ97W) with any questions.

